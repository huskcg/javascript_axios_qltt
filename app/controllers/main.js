const BASE_URL = "https://63bea80be348cb0762149ce3.mockapi.io";
document.getElementById("add").disabled = false;
// Call api get list user
// Function fecth data
function fecthUserList() {
  batLoading();
  axios({
    url: `${BASE_URL}/user`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      console.log(" res:", res.data);
      renderUserList(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err:", err);
    });
}
// First fetch / Reload page
fecthUserList();

// Delete user
function deleleUser(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/user/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      tatLoading();
      // fetch data after delete complete
      fecthUserList();
    })
    .catch(function (err) {
      tatLoading();
      console.log("err:", err);
    });
}

// Add user
function addUser() {
  batLoading();
  axios({
    url: `${BASE_URL}/user`,
    method: "POST",
    data: getDataFromForm(),
  })
    .then(function (res) {
      tatLoading();
      // fetch data after add complete
      fecthUserList();
    })
    .catch(function (err) {
      tatLoading();
      console.log("err:", err);
    });
}

// Edit user
function editUser(id) {
  batLoading();

  axios({
    url: `${BASE_URL}/user/${id}`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      document.getElementById("myModal").style.display = "block";
      document.getElementById("myModal").classList.add("show");
      document.getElementById("myModal").style.overflow = "auto";

      document.getElementById("add").disabled = true;
      document.getElementById("update").disabled = false;

      document.getElementById("title_id").style.display = "block";
      document.getElementById("id").disabled = true;
      console.log(res.data.id);
      ShowDataInForm(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

// Update user
function updateUser() {
  let data = getDataFromForm();
  let id = document.getElementById("id").value;
  batLoading();
  axios({
    url: `${BASE_URL}/user/${id}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      tatLoading();
      fecthUserList();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
